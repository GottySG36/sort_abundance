// Utility to sort an abundance table based on average rank

package main

import (
    "bufio"
    "flag"
    "fmt"
    "log"
    "os"
    "regexp"
    "strconv"
    "strings"
    "sync"

    "bitbucket.org/GottySG36/rank"
)

var (
    inp  = flag.String("i", "-", "Input abundance table to sort, defaults to stdin")
    outp = flag.String("o", "-", "Output directory in which to write the output files, defaults to stdout.")
    pref = flag.String("p", "sample", "Prefix used to build output file names.")
    cols = flag.String("columns", "", "Columns to sort, defaults to all")
    meta = flag.String("metadata", "taxonomy", "Specifies that there are some metadata in the table. Per defaults, it assumes the `taxonomy` is the last column")
    topX = flag.Int("top-x", -1, "Number of most abundant taxids to keep from the abundance table. Defaults to all")
    filt = flag.String("filter", "[Uu]nclassified,[Uu]nassigned", "Terms to ignore in metadata columns 'taxonomy', comma seperated (for topX selection only)")
    sep  = flag.String("d", "\t", "Field delimiter used. Defaults to `,`")
    maxJobs = flag.Int("t", 4, "Maximum number of concurrent jobs to run at a time")
)

// type used to store a column name as well as it's position in the table
type Item struct {
    Index   int
    Name    string
}

// Simple type definition to simplify some basic operations on lists
type List []*Item

// Two types of columns
// `data` columns       : Will contain the counts for the indiviual samples found in the table
// `metadata` columns   : Contains any type of extra information. This will usually be the taxonomy.
type Columns struct {
    Index       string
    Data        *List
    Metadata    *List
}

// Abundance table definition
// Table should contain a list of `Columns` to be manipulated. The `Data` columns must contain 
// floats corresponding to counts (or ratios, if relative) for a specific index (or taxonomic ID).
// There ae two ways ta index the table. 
// `TaxIndex` : A map that, for a given identifier, gives the row index, and
// `IndexTax` : A map that for a specific index, returns the associated identifier.
// The `Columns` also contain any form of `Metadata`.  
type Table struct {
    IndexTax    map[int]string
    TaxIndex    map[string]int
    Columns     Columns
    Data        [][]float64
    Metadata    [][]string
}

// Verifies if element is found in List l
func (l *List) contains(s *Item) int {
    for idx, i := range *l {
        if i.Name == s.Name {
            return idx
        }
    }
    return -1
}

// Create a List struct from a slice
func getList(s []string) *List {
    l := List{}
    for _, v := range s {
        l = append(l, &Item{Index:-1, Name:v})
    }
    return &l
}

func NewTable() *Table {
    var t Table
    t.IndexTax = make(map[int]string)
    t.TaxIndex = make(map[string]int)
    return &t
}

func (t *Table) getOrderedIndex() []string {
    ordered := make([]string, len(t.IndexTax))
    for i:= 0; i<len(t.IndexTax); i++ {
        ordered[i] = t.IndexTax[i]
    }
    return ordered
}

func (t *Table) getLine(i int) []string {
    line := make([]string, len(*t.Columns.Data)+len(*t.Columns.Metadata)+1)
    line[0] = t.IndexTax[i]
    for idx, col := range *t.Columns.Data {
        line[col.Index] = strconv.FormatFloat(t.Data[idx][i], 'f', -1, 64)
    }
    if len(*t.Columns.Metadata) != 0 {
        for idx, col := range *t.Columns.Metadata {
            line[col.Index] = t.Metadata[idx][i]
        }
    }
    return line
}


// Validate that all specified columns have been identified in the input table
func (l *List) hasMissing() ([]string, bool) {
    missing := make([]string, 0)
    hasMissing := false
    for _, v := range *l {
        if v.Index == -1 {
            hasMissing = true
            missing = append(missing, v.Name)
        }
    }
    return missing, hasMissing
}

// Helper function to generate the Columns struct from the first line of the table to sort
func (t *Table) addHeader(l []string, c, m *List) {
    head := getList(l)
    t.Columns.Index = (*head)[0].Name
    data := List{}
    metadata := List{}
    for idx, h := range (*head)[1:] {
        h.Index = idx+1
        if c.contains(h) != -1 {
            data = append(data, h)
            t.Data = append(t.Data, make([]float64, 0))
        } else if m.contains(h) != -1{
            metadata = append(metadata, h)
            t.Metadata = append(t.Metadata, make([]string, 0))
        } else {
            //log.Printf("INFO -:- Column `%v` was not specified and will be considered as a data column.\n", h)
            data = append(data, h)
            t.Data = append(t.Data, make([]float64, 0))
        }
    }
    t.Columns.Data = &data
    t.Columns.Metadata = &metadata
}

// Helper function to apply to every single line in the table to sort.
// Adds to row index to the Table Struct
func (t *Table) addRowIndex(r []string) {
    t.IndexTax[len(t.IndexTax)] = r[0]
    t.TaxIndex[r[0]] = len(t.TaxIndex)
}

// Helper function to apply to every single line in the table to sort.
// Adds the data (excluding any metadata) for that row
func (t *Table) addRowData(r []string) error {
    for idx, col := range *(t.Columns.Data) {
        d, err := strconv.ParseFloat(r[col.Index], 64)
        if err != nil {
            return err
        }
        t.Data[idx] = append(t.Data[idx], d)
    }
    return nil
}

// Helper function to apply to every single line in the table to sort.
// Adds the metadata for that row
func (t *Table) addRowMetadata(r []string) {
    for idx, col := range *(t.Columns.Metadata) {
        t.Metadata[idx] = append(t.Metadata[idx], r[col.Index])
    }
}

func (t *Table) String() string {
    var b strings.Builder

    // Writing header into builder
    b.WriteString(t.Columns.Index)
    b.WriteString("\t")

    // Writing individual column names
    for cidx := 0; cidx < len(*(t.Columns.Data)) - 1; cidx++ {
        b.WriteString((*t.Columns.Data)[cidx].Name)
        b.WriteString("\t")
    }
    b.WriteString((*t.Columns.Data)[len(*(t.Columns.Data))-1].Name)

    switch n := len(*(t.Columns.Metadata)); n{
    case 0:
        b.WriteString("\n")

    case 1:
        b.WriteString("\t")
        b.WriteString((*t.Columns.Metadata)[0].Name)
        b.WriteString("\n")

    default:
        b.WriteString("\t")
        for cidx := 0; cidx < len(*t.Columns.Metadata) - 1; cidx++ {
            b.WriteString((*t.Columns.Metadata)[cidx].Name)
        }
        b.WriteString((*t.Columns.Metadata)[len(*t.Columns.Metadata)-1].Name)
        b.WriteString("\n")
    }

    // Writing individual rows
    for ridx := 0; ridx < len(t.IndexTax); ridx++ {
        b.WriteString(t.IndexTax[ridx])
        b.WriteString("\t")

        for cidx := 0; cidx < len(*t.Columns.Data) - 1; cidx++ {
            b.WriteString(strconv.FormatFloat(t.Data[cidx][ridx], 'f', -1, 64))
            b.WriteString("\t")
        }
        b.WriteString(strconv.FormatFloat(t.Data[len(*t.Columns.Data)-1][ridx], 'f', -1, 64))

        switch n := len(*t.Columns.Metadata); n {
        case 0:
            b.WriteString("\n")

        case 1:
            b.WriteString("\t")
            b.WriteString(t.Metadata[0][ridx])
            b.WriteString("\n")

        default:
            b.WriteString("\t")
            for cidx := 0; cidx < len(*t.Columns.Metadata) - 1; cidx++ {
                b.WriteString(t.Metadata[cidx][ridx])
            }
            b.WriteString(t.Metadata[len(*t.Columns.Metadata)-1][ridx])
            b.WriteString("\n")
        }
    }
    return b.String()
}

// Generates the abundance table to sort from the input file
func LoadTable(i, c, m, sep string) (*Table, error) {
    cols    := getList(strings.Split(c, ","))
    metad   := getList(strings.Split(m, ","))

    var f *os.File
    var err error
    if i != "-" {
        f, err = os.Open(i)
    } else {
        f = os.Stdin
    }
    defer f.Close()
    if err != nil {
        return nil, err
    }
    tbl := NewTable()
    s := bufio.NewScanner(f)
    s.Scan()
    tbl.addHeader(strings.Split(s.Text(), sep), cols, metad)

    for s.Scan() {
        if s.Text() == "" {continue}

        line := strings.Split(s.Text(), sep)

        tbl.addRowIndex(line)
        err := tbl.addRowData(line)
        if err != nil {
            return nil, err
        }
        tbl.addRowMetadata(line)
    }
    return tbl, nil
}

func (t *Table) GetTopX(n int, r rank.Ranks, filter string) (rank.Ranks, error) {
    comma, _ := regexp.Compile(`,`)
    regFilt,  err := regexp.Compile(fmt.Sprintf(`%v`, comma.ReplaceAllString(filter,  "|")))
    if err != nil {
        return nil, err
    }
    var rk rank.Ranks
    var counter int
    for i:= 0; i < n; i++ {
        if pos := t.Columns.Metadata.contains(&Item{Name:"taxonomy"}); pos != -1{
            if regFilt.MatchString(t.Metadata[pos][counter]) {
                i--
                counter++
                continue
            }
        }
        rk = append(rk, r[counter])
        counter++
    }
    return rk, nil
}

func main() {
    flag.Parse()
    threads := make(chan struct{}, *maxJobs)

    tbl, err := LoadTable(*inp, *cols, *meta, *sep)
    if err != nil {
        log.Fatalf("Error -:- %v\n", err)
    }

    sortedTaxo := make(map[string]rank.Counts, len(*tbl.Columns.Data))
    avgRank := make(map[string]float64)
    nbAvgRank := make(map[string]float64)

    var lock sync.Mutex
    var wg sync.WaitGroup

    for idx, sample := range *tbl.Columns.Data {
        wg.Add(1)
        go func(t *Table, s *Item, pos int) {
            defer wg.Done()

            threads <- struct{}{}
            srt := rank.SortAbundance(t.getOrderedIndex(), t.Data[pos])

            lock.Lock()
            sortedTaxo[s.Name] = srt
            lock.Unlock()
            <-threads
        }(tbl, sample, idx)
    }

    wg.Wait()

    for _, c := range sortedTaxo {
        for _, taxo := range c {
            avgRank[taxo.Taxid] += float64(taxo.Rank)
            nbAvgRank[taxo.Taxid] += 1
        }
    }

    for t, _ := range avgRank {
       avgRank[t] /= nbAvgRank[t]
    }
    t20Sort := rank.SortRanks(avgRank)

    if *topX > len(t20Sort) || *topX <= 0 {
        *topX = len(t20Sort)
    }

    sTbl := NewTable()
    sTbl.Columns = tbl.Columns
    for _, _ = range *sTbl.Columns.Data {
        sTbl.Data = append(sTbl.Data, make([]float64, 0))
    }
    for _, _ = range *sTbl.Columns.Metadata {
        sTbl.Metadata = append(sTbl.Metadata, make([]string, 0))
    }
    for _, val := range t20Sort {
        sTbl.addRowIndex(tbl.getLine(tbl.TaxIndex[val.Taxid]))
        sTbl.addRowData(tbl.getLine(tbl.TaxIndex[val.Taxid]))
        sTbl.addRowMetadata(tbl.getLine(tbl.TaxIndex[val.Taxid]))
    }

    var o *os.File
    if *outp != "-" {
        path := fmt.Sprintf("%v/%v-sorted-table.tsv", *outp, *pref)
        o, err = os.Create(path)
        if err != nil {
            log.Fatalf("Error -:- %v\n", err)
        }
    } else {
        o = os.Stdout
    }
    defer o.Close()
    fmt.Fprintf(o, "%v", sTbl)

    topXrank, err := sTbl.GetTopX(*topX, t20Sort, *filt)
    var or *os.File
    if *outp != "-" {
        path := fmt.Sprintf("%v/%v-rank.csv", *outp, *pref)
        or, err = os.Create(path)
        if err != nil {
            log.Fatalf("Error -:- %v\n", err)
        }
    } else {
        path := fmt.Sprintf("top-%v-rank.csv", *topX)
        or, err = os.Create(path)
        if err != nil {
            log.Fatalf("Error -:- %v\n", err)
        }
    }
    defer or.Close()
    for _, val := range topXrank[:len(topXrank)-1] {
        fmt.Fprintf(or, "%v\n", val)
    }
    fmt.Fprintf(or, "%v", topXrank[len(topXrank)-1])


}
